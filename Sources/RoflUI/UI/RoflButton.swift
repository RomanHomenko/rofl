//
//  RoflButton.swift
//  RoflUI
//
//  Created by r.khomenko on 29.11.2022.
//

import Foundation
import UIKit

final public class RoflButton: GestureHandler {
	private var label: UILabel = UILabel()
	
	public override var isHighlighted: Bool {
		didSet {
			let highlighted = UIColor.orange.withAlphaComponent(0.8)
			let normal = UIColor.orange
			backgroundColor = isHighlighted ? highlighted : normal
		}
	}
	
	public init() {
		super.init(frame: .zero)
		
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public func setTitle(_ title: String) {
		label.text = title
	}
	
	public override func addAction(_ action: @escaping () -> Void) {
		self.action = action
	}
	
	private func setupViews() {
		self.layer.cornerRadius = CGFloat(20)
		
		label.text = ""
		label.font = UIFont.systemFont(ofSize: 24)
		label.textColor = UIColor.black
		label.textAlignment = .center
		
		backgroundColor = .orange
		addSubview(label)
	}
	
	private func setupConstraints() {
		NSLayoutConstraint.activate([
			self.heightAnchor.constraint(equalToConstant: 40),
			self.widthAnchor.constraint(equalToConstant: 120),
		])
		
		label.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			label.topAnchor.constraint(equalTo: self.topAnchor),
			label.leftAnchor.constraint(equalTo: self.leftAnchor),
			label.rightAnchor.constraint(equalTo: self.rightAnchor),
			label.bottomAnchor.constraint(equalTo: self.bottomAnchor),
		])
	}
}

